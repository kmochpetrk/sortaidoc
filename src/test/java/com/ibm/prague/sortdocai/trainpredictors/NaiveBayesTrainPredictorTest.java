package com.ibm.prague.sortdocai.trainpredictors;

import com.ibm.prague.sortdocai.entities.Project;
import com.ibm.prague.sortdocai.entities.TrainTestCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class NaiveBayesTrainPredictorTest {

    @Autowired
    private NaiveBayesTrainPredictor naiveBayesTrainPredictor;

    @Test
    void train() {
        Project project = new Project();
        project.setId(1L);
        List<String> labels = new ArrayList<>();
        labels.add("fa");
        labels.add("ppd");
        labels.add("vpd");
        labels.add("prijemka");
        labels.add("vydejka");
        project.setLabels(labels);
        project.setTrainCases(new ArrayList<>());

        TrainTestCase trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("fa");
        trainTestCase.setFullFilename("e:\\pokus\\f1.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("fa");
        trainTestCase.setFullFilename("e:\\pokus\\f2.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("ppd");
        trainTestCase.setFullFilename("e:\\pokus\\ppd1.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("ppd");
        trainTestCase.setFullFilename("e:\\pokus\\ppd2.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("vpd");
        trainTestCase.setFullFilename("e:\\pokus\\vpd1.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("vpd");
        trainTestCase.setFullFilename("e:\\pokus\\vpd2.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("prijemka");
        trainTestCase.setFullFilename("e:\\pokus\\prijemka1.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("prijemka");
        trainTestCase.setFullFilename("e:\\pokus\\prijemka2.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("vydejka");
        trainTestCase.setFullFilename("e:\\pokus\\vydejka1.txt");
        project.getTrainCases().add(trainTestCase);

        trainTestCase = new TrainTestCase();
        trainTestCase.setLabel("vydejka");
        trainTestCase.setFullFilename("e:\\pokus\\vydejka2.txt");
        project.getTrainCases().add(trainTestCase);


        naiveBayesTrainPredictor.train(project);

    }
}