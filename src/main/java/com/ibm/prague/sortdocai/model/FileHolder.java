package com.ibm.prague.sortdocai.model;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Component
public class FileHolder {
    private Map<String, String> mapOfFiles = new HashMap<>();


    public boolean containsKey(Object key) {
        return mapOfFiles.containsKey(key);
    }

    public String get(String key) {
        return mapOfFiles.get(key);
    }

    public String put(String key, String value) {
        return mapOfFiles.put(key, value);
    }
}
