package com.ibm.prague.sortdocai.model;

import java.util.*;

public class NaiveBayesModel extends  AbstractModel {
    private ModelType modelType = ModelType.NAIVE_BAYES;
    private Long projectId;
    private List<String> classes;
    private Set<String> vocabSet = new HashSet<>();
    private Map<String, Double> probsByClass = new HashMap<>();
    private Map<ValueClassWord, Double> probsByWordLabel = new HashMap<>();
    //private Map<String, Double> probsByWord = new HashMap<>();
    private Map<String,Map<String, Long>> mapOcurrByClass = new HashMap<>();
    private long totalNumberOfWords = 0L;
    private Map<String, Long> mapOfNumOfDocsByClass = new HashMap<>();
    private long totalNumOfDocs = 0L;
    private Map<String, Long> wordOccurrMap = new HashMap<>();

    public ModelType getModelType() {
        return modelType;
    }

    public void setModelType(ModelType modelType) {
        this.modelType = modelType;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
        this.classes.forEach(cl -> {
            mapOcurrByClass.put(cl, new HashMap<>());
            mapOfNumOfDocsByClass.put(cl, 0L);
        });
        this.totalNumberOfWords = this.classes.size();
    }

    public Map<String, Double> getProbsByClass() {
        return probsByClass;
    }

    public void setProbsByClass(Map<String, Double> probsByClass) {
        this.probsByClass = probsByClass;
    }

    public Map<ValueClassWord, Double> getProbsByWordLabel() {
        return probsByWordLabel;
    }

    public void setProbsByWordLabel(Map<ValueClassWord, Double> probsByWordLabel) {
        this.probsByWordLabel = probsByWordLabel;
    }

//    public Map<String, Double> getProbsByWord() {
//        return probsByWord;
//    }
//
//    public void setProbsByWord(Map<String, Double> probsByWord) {
//        this.probsByWord = probsByWord;
//    }

    public Set<String> getVocabSet() {
        return vocabSet;
    }

    public void setVocabSet(Set<String> vocabSet) {
        this.vocabSet = vocabSet;
    }

    public Map<String, Map<String, Long>> getMapOcurrByClass() {
        return mapOcurrByClass;
    }

    public void setMapOcurrByClass(Map<String, Map<String, Long>> mapOcurrByClass) {
        this.mapOcurrByClass = mapOcurrByClass;
    }

    public long getTotalNumberOfWords() {
        return totalNumberOfWords;
    }

    public void setTotalNumberOfWords(long totalNumberOfWords) {
        this.totalNumberOfWords = totalNumberOfWords;
    }

    public Map<String, Long> getMapOfNumOfDocsByClass() {
        return mapOfNumOfDocsByClass;
    }

    public void setMapOfNumOfDocsByClass(Map<String, Long> mapOfNumOfDocsByClass) {
        this.mapOfNumOfDocsByClass = mapOfNumOfDocsByClass;
    }

    public long getTotalNumOfDocs() {
        return totalNumOfDocs;
    }

    public void setTotalNumOfDocs(long totalNumOfDocs) {
        this.totalNumOfDocs = totalNumOfDocs;
    }

    public Map<String, Long> getWordOccurrMap() {
        return wordOccurrMap;
    }

    public void setWordOccurrMap(Map<String, Long> wordOccurrMap) {
        this.wordOccurrMap = wordOccurrMap;
    }
}
