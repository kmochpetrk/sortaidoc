package com.ibm.prague.sortdocai.model;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class ModelHolder {

    private Map<Long, NaiveBayesModel> modelByProjectId = new HashMap<>();

    public NaiveBayesModel get(Long key) {
        return modelByProjectId.get(key);
    }

    public NaiveBayesModel put(Long key, NaiveBayesModel value) {
        return modelByProjectId.put(key, value);
    }

    public NaiveBayesModel remove(Long key) {
        return modelByProjectId.remove(key);
    }

    public Collection<NaiveBayesModel> values() {
        return modelByProjectId.values();
    }
}
