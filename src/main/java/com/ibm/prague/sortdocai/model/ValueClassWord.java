package com.ibm.prague.sortdocai.model;

import lombok.Value;

@Value
public class ValueClassWord {
    private String label;
    private String word;
}
