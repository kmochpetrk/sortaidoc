package com.ibm.prague.sortdocai.entities;

import com.ibm.prague.sortdocai.dtos.TrainTestCaseDto;
import lombok.Data;

@Data
public class TrainTestCase {
    private String label;
    private String fullFilename;

    public static TrainTestCaseDto mapToDto(TrainTestCase trainTestCase) {
        TrainTestCaseDto trainTestCaseDto = new TrainTestCaseDto(trainTestCase.label, trainTestCase.fullFilename);
        return trainTestCaseDto;
    }
}
