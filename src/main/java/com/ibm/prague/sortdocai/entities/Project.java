package com.ibm.prague.sortdocai.entities;

import com.ibm.prague.sortdocai.dtos.ProjectDto;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class Project {
    private Long id;

    private String name;
    private List<String> labels;
    private List<TrainTestCase> trainCases;
    private List<TrainTestCase> testCases;


    public static ProjectDto mapDto(Project project) {
        ProjectDto projectDto = new ProjectDto(
                project.name,
                project.labels,
                project.trainCases != null ? project.trainCases.stream().map(TrainTestCase::mapToDto).collect(Collectors.toList()) : null,
                project.testCases != null ? project.testCases.stream().map(TrainTestCase::mapToDto).collect(Collectors.toList()): null,
                project.getId());

        return projectDto;
    }
}
