package com.ibm.prague.sortdocai.trainpredictors;

import com.ibm.prague.sortdocai.entities.Project;
import com.ibm.prague.sortdocai.entities.TrainTestCase;
import com.ibm.prague.sortdocai.model.FileHolder;
import com.ibm.prague.sortdocai.model.ModelHolder;
import com.ibm.prague.sortdocai.model.NaiveBayesModel;
import com.ibm.prague.sortdocai.model.ValueClassWord;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

@Component
public class NaiveBayesTrainPredictor implements TrainPredictor{

    @Autowired
    private ModelHolder modelHolder;

    @Autowired
    private FileHolder fileHolder;

    @Override
    public Project train(Project project) {
        NaiveBayesModel model = new NaiveBayesModel();
        model.setClasses(project.getLabels());
        model.setProjectId(project.getId());
        for (TrainTestCase trainTestCase : project.getTrainCases()) {
            String fullFilename = trainTestCase.getFullFilename();
            LoggerFactory.getLogger(NaiveBayesTrainPredictor.class).info("Train process " + fullFilename);
//            String fullFileNamaNew = fullFilename.replace("\\", "/");
//            final Path path = Paths.get(fullFileNamaNew);
            String fileAsString = fileHolder.get(fullFilename);
//            try {
//                byte[] bytes = Files.readAllBytes(path);
//                fileAsString = new String(bytes, "utf-8");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            final StringTokenizer stringTokenizer = new StringTokenizer(fileAsString);
            while(stringTokenizer.hasMoreTokens()) {
                final String nextWord = stringTokenizer.nextToken();
                if (nextWord.length() >= 4) {
                    model.getClasses().forEach(cl -> {
                        Map<String, Long> stringLongMap = model.getMapOcurrByClass().get(cl);
                        Assert.notNull(stringLongMap);
                        final Long aLong = stringLongMap.get(nextWord);
                        if (aLong == null) {
                            stringLongMap.put(nextWord, 1L);
                        }
                        if (cl.equals(trainTestCase.getLabel())) {
                            stringLongMap.put(nextWord, stringLongMap.get(nextWord) + 1);
                        }
                    });
                    model.setTotalNumberOfWords(model.getTotalNumberOfWords() + 1);
                    final Long numOfWordsByWord = model.getWordOccurrMap().get(nextWord);
                    if (numOfWordsByWord == null) {
                        model.getWordOccurrMap().put(nextWord, 0L);
                    }
                    model.getWordOccurrMap().put(nextWord, model.getWordOccurrMap().get(nextWord) + 1);
                }
            }
            model.setTotalNumOfDocs(model.getTotalNumOfDocs() + 1);
            model.getMapOfNumOfDocsByClass().put(trainTestCase.getLabel(), model.getMapOfNumOfDocsByClass().get(trainTestCase.getLabel()) + 1);
        }
        model.getClasses().forEach(cl -> {
            model.getProbsByClass().put(cl,Math.abs(Math.log(model.getMapOfNumOfDocsByClass().get(cl)/(double)model.getTotalNumOfDocs())));
            model.getMapOcurrByClass().get(cl).forEach((word, num) -> {
                model.getProbsByWordLabel().put(new ValueClassWord(cl, word), Math.abs(Math.log(num/(double) model.getTotalNumberOfWords())));
            });
        });
        modelHolder.put(project.getId(), model);
        return project;
    }

    @Override
    public Project test(Project project) {

        NaiveBayesModel model = modelHolder.get(project.getId());

        Assert.notNull(model);

        for (TrainTestCase trainTestCase : project.getTestCases()) {
            Long totalNumOWordsInDoc = 0L;

            Map<String, Double> resultByLabel = new HashMap<>();
            project.getLabels().forEach((label) -> {
                resultByLabel.put(label, 1.0);
            });

                String fullFilename = trainTestCase.getFullFilename();
                LoggerFactory.getLogger(NaiveBayesTrainPredictor.class).info("Train process " + fullFilename);
//                String fullFileNamaNew = fullFilename.replace("\\", "/");
//                final Path path = Paths.get(fullFileNamaNew);
                String fileAsString = fileHolder.get(fullFilename);
//                try {
//                    byte[] bytes = Files.readAllBytes(path);
//                    fileAsString = new String(bytes, "utf-8");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                final StringTokenizer stringTokenizer = new StringTokenizer(fileAsString);
                while(stringTokenizer.hasMoreTokens()) {
                    final String word = stringTokenizer.nextToken();
                    if (word.length() >= 4) {
                        project.getLabels().forEach((cl) -> {
                            final ValueClassWord key = new ValueClassWord(cl, word);
                            final Double aDouble = model.getProbsByWordLabel().get(key) != null ? model.getProbsByWordLabel().get(key) : 1.0;
                            resultByLabel.put(cl, resultByLabel.get(cl) * aDouble);
                        });
                        totalNumOWordsInDoc += (model.getWordOccurrMap().get(word) != null ? model.getWordOccurrMap().get(word) : 0L);
                    }
                }
                final long totalNumOWordsDoc2 = totalNumOWordsInDoc;
            project.getLabels().forEach((cl) -> {
                resultByLabel.put(cl, resultByLabel.get(cl) * model.getProbsByClass().get(cl));
                resultByLabel.put(cl, resultByLabel.get(cl) /(totalNumOWordsDoc2 / (double)model.getTotalNumberOfWords()));
            });

            String maxClass = project.getLabels().get(0);
            for(int i = 1; i < project.getLabels().size(); i++) {
                if (resultByLabel.get(project.getLabels().get(i)) < resultByLabel.get(maxClass)) {
                    maxClass = project.getLabels().get(i);
                }
            }
            trainTestCase.setLabel(maxClass);
        }
        return project;
    }
}
