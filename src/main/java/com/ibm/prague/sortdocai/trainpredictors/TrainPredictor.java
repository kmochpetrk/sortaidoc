package com.ibm.prague.sortdocai.trainpredictors;

import com.ibm.prague.sortdocai.entities.Project;

public interface TrainPredictor {
    Project train(Project project);

    Project test(Project project);
}
