package com.ibm.prague.sortdocai;

import com.ibm.prague.sortdocai.dtos.ProjectDto;
import com.ibm.prague.sortdocai.entities.Project;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class InMemoryStore {

    private long sequence = 1;

    private List<Project> projects = new ArrayList<>();


    public synchronized Project add(Project project) {
        project.setId(sequence++);
        projects.add(project);
        return project;
    }

    public boolean remove(Project project) {
        return projects.remove(project);
    }

    public synchronized Project updateProjectById(Long id, Project project) {
        int indexToFind = -1;
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId().equals(id)) {
                indexToFind = i;
                break;
            }
        }
        if (indexToFind == -1) {
            throw new IllegalStateException("project " + id + " nenalezen.");
        }
        projects.set(indexToFind, project);
        return project;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Project getProjectById(Long id) {
        final List<Project> collect = this.projects.stream().filter(e -> e.getId().equals(id)).collect(Collectors.toList());


        return collect.get(0);
    }
}
