package com.ibm.prague.sortdocai.controllers;

import com.ibm.prague.sortdocai.dtos.ProjectDto;
import com.ibm.prague.sortdocai.model.FileHolder;
import com.ibm.prague.sortdocai.services.ProjectService;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@RestController
@CrossOrigin
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private FileHolder fileHolder;

    @RequestMapping(value = "/projects", produces = "application/json")
    public ResponseEntity<List<ProjectDto>> getProjects() {
        List<ProjectDto> projectsToReturn = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("content-type", "application/json");
        try {
           projectsToReturn = projectService.getProjects();
        } catch (Exception ex) {
            LoggerFactory.getLogger(ProjectController.class).info("Nastala chyba ", ex);
            return new ResponseEntity<List<ProjectDto>>(new ArrayList<>(), httpHeaders,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
//        projectsToReturn.add(createProject1());
//        projectsToReturn.add(createProject2());
        return new ResponseEntity<List<ProjectDto>>(projectsToReturn, httpHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable Long id, @RequestBody ProjectDto projectDto) {
        ProjectDto projectDto1 = null;
        try {
            projectDto1 = projectService.updateProject(id, projectDto);
        } catch (Exception exc) {
            LoggerFactory.getLogger(ProjectController.class).info("Nastala chyba ", exc);
            return new ResponseEntity<ProjectDto>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<ProjectDto>(projectDto1, HttpStatus.OK);
    }

    @RequestMapping(value = "/projects", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<ProjectDto> createProject(@RequestBody ProjectDto projectDto) {
        ProjectDto projectDtoReturn = null;
        try {
            projectService.saveProject(projectDto);
        } catch (Exception ex) {
            LoggerFactory.getLogger(ProjectController.class).info("Nastala chyba ", ex);
            return new ResponseEntity<ProjectDto>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<ProjectDto>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/projects/train")
    public ResponseEntity<ProjectDto> trainProjectById(@RequestParam Long id) {
        ProjectDto projectTrained = this.projectService.trainModelByProjectId(id);
        return new ResponseEntity<ProjectDto>(projectTrained, HttpStatus.OK);
    }

    @RequestMapping(value = "/projects/test")
    public ResponseEntity<ProjectDto> testProjectById(@RequestParam Long id) {
        ProjectDto projectTrained = this.projectService.testModelByProjectId(id);
        return new ResponseEntity<ProjectDto>(projectTrained, HttpStatus.OK);
    }

    @RequestMapping(value="/projects/upload", method = RequestMethod.POST)
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file){
        String text = null;
        try {
            text = IOUtils.toString(file.getInputStream(), StandardCharsets.UTF_8.name());
            final int i = new StringTokenizer(text).countTokens();
            LoggerFactory.getLogger(ProjectController.class).info("Pocet slov " + i);
        } catch (IOException e) {
            LoggerFactory.getLogger(ProjectController.class).info("Error", e);
            new ResponseEntity("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        fileHolder.put(file.getOriginalFilename(), text);
        return new ResponseEntity("OK", HttpStatus.OK);
    }

    @RequestMapping(value="/projects/uploadpdf", method = RequestMethod.POST)
    public ResponseEntity<String> uploadFilePdf(@RequestParam("file") MultipartFile file){
        String text = null;
        try {

            final InputStream inputStream = file.getInputStream();
            final PDDocument document = PDDocument.load(inputStream);
            String pdfFileInText = null;

            if (!document.isEncrypted()) {

                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);

                PDFTextStripper tStripper = new PDFTextStripper();

                pdfFileInText = tStripper.getText(document);
                //System.out.println("Text:" + st);

                // split by whitespace
                String lines[] = pdfFileInText.split("\\r?\\n");
                for (String line : lines) {
                    System.out.println(line);
                }

            }

            text = pdfFileInText; //IOUtils.toString(file.getInputStream(), StandardCharsets.UTF_8.name());
            final int i = new StringTokenizer(text).countTokens();
            LoggerFactory.getLogger(ProjectController.class).info("Pocet slov " + i);
        } catch (IOException e) {
            LoggerFactory.getLogger(ProjectController.class).info("Error", e);
            new ResponseEntity("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        fileHolder.put(file.getOriginalFilename(), text);
        return new ResponseEntity("OK", HttpStatus.OK);
    }

    private ProjectDto createProject1() {
        List<String> categs = new ArrayList<String>();
        categs.add("accept");
        categs.add("offer");
        return new ProjectDto("Project 1", categs, null, null, 1L);
    }

    private ProjectDto createProject2() {
        List<String> categs = new ArrayList<String>();
        categs.add("1");
        categs.add("2");
        return new ProjectDto("Project 2", categs, null, null, 2L);
    }

}
