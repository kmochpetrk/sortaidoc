package com.ibm.prague.sortdocai.dtos;

import com.ibm.prague.sortdocai.entities.TrainTestCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainTestCaseDto {
    private String label;
    private String fullFilename;

    public static TrainTestCase mapToDb(TrainTestCaseDto trainTestCaseDto) {
        final TrainTestCase trainTestCase = new TrainTestCase();
        trainTestCase.setFullFilename(trainTestCaseDto.fullFilename);
        trainTestCase.setLabel(trainTestCaseDto.getLabel());
        return trainTestCase;
    }
}
