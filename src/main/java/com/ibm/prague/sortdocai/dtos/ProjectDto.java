package com.ibm.prague.sortdocai.dtos;

import com.ibm.prague.sortdocai.entities.Project;
import com.ibm.prague.sortdocai.entities.TrainTestCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDto {
    private String name;
    private List<String> labels;
    private List<TrainTestCaseDto> trainCases;
    private List<TrainTestCaseDto> testCases;
    private Long id;

    public static Project mapToDb(ProjectDto projectDto) {
        Project project = new Project();
        project.setLabels(projectDto.getLabels());
        project.setName(projectDto.getName());
        project.setTestCases(projectDto.getTestCases() != null ? projectDto.getTestCases().stream().map(TrainTestCaseDto::mapToDb).collect(Collectors.toList()): null);
        project.setTrainCases(projectDto.getTrainCases() != null ? projectDto.getTrainCases().stream().map(TrainTestCaseDto::mapToDb).collect(Collectors.toList()): null);
        project.setId(projectDto.getId());
        return project;
    }
}
