package com.ibm.prague.sortdocai.services;

import com.ibm.prague.sortdocai.InMemoryStore;
import com.ibm.prague.sortdocai.dtos.ProjectDto;
import com.ibm.prague.sortdocai.entities.Project;
import com.ibm.prague.sortdocai.trainpredictors.TrainPredictor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    private InMemoryStore inMemoryStore;

    @Autowired
    private TrainPredictor trainPredictor;

    public List<ProjectDto> getProjects() {
        return inMemoryStore.getProjects().stream().map(Project::mapDto).collect(Collectors.toList());
    }

    public ProjectDto saveProject(ProjectDto projectDto) {
        final ProjectDto projectDto1 = Project.mapDto(inMemoryStore.add(ProjectDto.mapToDb(projectDto)));
        return projectDto1;
    }

    public ProjectDto updateProject(Long id, ProjectDto projectDto) {
        final Project project1 = inMemoryStore.updateProjectById(id, ProjectDto.mapToDb(projectDto));

        return Project.mapDto(project1);

    }

    public ProjectDto trainModelByProjectId(Long id) {

        Project project = inMemoryStore.getProjectById(id);
        trainPredictor.train(project);
        return Project.mapDto(project);
    }

    public ProjectDto testModelByProjectId(Long id) {

        Project project = inMemoryStore.getProjectById(id);
        final Project test = trainPredictor.test(project);
        return Project.mapDto(test);
    }
}
