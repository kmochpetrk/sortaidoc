package com.ibm.prague.sortdocai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SortDocAiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SortDocAiApplication.class, args);
    }

}
