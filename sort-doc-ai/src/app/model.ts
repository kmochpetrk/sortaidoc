class SelectableElement {


  constructor(description: string, id: number, label: string) {
    this.description = description;
    this.id = id;
    this.label = label;
  }

  description: string;
  id: number;
  label: string;
}

class Project {
  id: number;
  name: string;
  labels: string[];
  trainCases?: TrainTestCase[];
  testCases?: TrainTestCase[];
}

class TrainTestCase {


  constructor(label: string, fullFilename: string) {
    this.label = label;
    this.fullFilename = fullFilename;
  }

  label: string;
  fullFilename: string;
}

export { SelectableElement, Project, TrainTestCase }
