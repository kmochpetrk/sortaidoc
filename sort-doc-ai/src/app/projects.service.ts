import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Project} from "./model";
import {environment} from "../environments/environment";

@Injectable()
export class ProjectsService {

  constructor(private httpClient: HttpClient) { }

  public getProjects(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(environment.base_url + '/projects');
  }

  public createProject(project: Project): Observable<Project> {

    return this.httpClient.post<Project>(environment.base_url + '/projects', project,
      {headers: {'content-type': 'application/json'}});
  }

  public updateProject(project: Project): Observable<Project> {

    return this.httpClient.put<Project>(environment.base_url + '/projects' + '/' + project.id, project,
      {headers: {'content-type': 'application/json'}});
  }

  public testProject(project: Project): Observable<Project> {

    return this.httpClient.get<Project>(environment.base_url + '/projects/test',
      {headers: {'content-type': 'application/json'}, params: {id: String(project.id)}});
  }

  public trainProject(project: Project): Observable<Project> {

    return this.httpClient.get<Project>(environment.base_url + '/projects/train',
      {headers: {'content-type': 'application/json'}, params: {id: String(project.id)}});
  }

  uploadFile(formData: FormData): Observable<string> {
    return this.httpClient.post<string>(environment.base_url + "/projects/upload", formData);
  }

  uploadFilePdf(formData: FormData): Observable<string> {
    return this.httpClient.post<string>(environment.base_url + "/projects/uploadpdf", formData);
  }
}
