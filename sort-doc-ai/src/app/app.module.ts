import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MiddleCardComponent } from './middle-card/middle-card.component';
import { HeaderCardComponent } from './header-card/header-card.component';
import { FooterCardComponent } from './footer-card/footer-card.component';
import { MiddleTitleButtonComponent } from './middle-title-button/middle-title-button.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ProjectsService} from "./projects.service";

@NgModule({
  declarations: [
    AppComponent,
    MiddleCardComponent,
    HeaderCardComponent,
    FooterCardComponent,
    MiddleTitleButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ProjectsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
