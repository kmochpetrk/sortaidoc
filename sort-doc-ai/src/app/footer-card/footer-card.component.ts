import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-footer-card',
  templateUrl: './footer-card.component.html',
  styleUrls: ['./footer-card.component.css']
})
export class FooterCardComponent implements OnInit {

  @Output()
  saveEvent = new EventEmitter();

  @Output()
  trainEvent = new EventEmitter();

  @Output()
  testEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  pushedButtonTrain() {
    console.log("train pushed");
    this.trainEvent.emit();
  }

  pushedButtonTest() {
    console.log("test pushed");
    this.testEvent.emit();
  }

  pushedButtonUlozit() {
    console.log("save pushed");
    this.saveEvent.emit();
  }
}
