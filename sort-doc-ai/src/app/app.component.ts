import {Component, OnInit, ViewChild} from '@angular/core';
import {MiddleCardComponent} from "./middle-card/middle-card.component";
import {ProjectsService} from "./projects.service";
import {Project, SelectableElement, TrainTestCase} from "./model";
import {Observable} from "rxjs/internal/Observable";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'sort-doc-ai';
  addModalVisible = false;
  addModalProjectVisible = false;
  addedCategory: string;
  readProjectModel: Project[] = [];

  constructor(private projectsService: ProjectsService) {

  }

  @ViewChild('project', {static: false})
  middleCardComponentProject: MiddleCardComponent;

  @ViewChild('category', {static: false})
  middleCardComponent: MiddleCardComponent;

  @ViewChild('uceni', {static: false})
  middleCardComponentLearn: MiddleCardComponent;

  @ViewChild('test', {static: false})
  middleCardComponentTest: MiddleCardComponent;

  addedProject: string;
  selectedProject: SelectableElement;

  pushbuttn(msg: string) {
//    window.alert(msg);
    if (msg === 'category') {
      this.addModalVisible = true;
    }

    if (msg === 'project') {
      this.addModalProjectVisible = true;
    }
  }

  addCategory() {
    this.middleCardComponent.addElement(new SelectableElement(this.addedCategory, null, null));
    this.addModalVisible = false;
    this.middleCardComponentLearn.modelCategoriesChanged(this.middleCardComponent.getElements().map(se => se.description));
  }

  closeCategory() {
    this.addModalVisible = false;
  }

  pushTimesButtonDeleteCat($event: SelectableElement[]) {
    console.log('pushTimesButtonDeleteCat' + ($event.length > 0) ? $event[0] : '');
    this.middleCardComponentLearn.modelCategoriesChanged($event.map(se => se.description));
  }

  selectElemChangedProject($event: SelectableElement) {
    const filteredModel = this.readProjectModel.filter(project => project.name === $event.description);
    if (filteredModel && filteredModel.length > 0) {
      const newLabels = (filteredModel[0].labels ? filteredModel[0].labels : []);
      this.middleCardComponent.setAllElements(newLabels.map(v => new SelectableElement(v, null, null)))
      this.middleCardComponentLearn.modelCategoriesChanged(newLabels)
      this.middleCardComponentTest.setAllElements((filteredModel[0].testCases && filteredModel[0].testCases.length > 0) ? filteredModel[0].testCases.map(e => new SelectableElement(e.fullFilename, null, e.label)) : []);
      this.middleCardComponentLearn.setAllElements((filteredModel[0].trainCases && filteredModel[0].trainCases.length > 0) ? filteredModel[0].trainCases.map(e => new SelectableElement(e.fullFilename, null, e.label)) : []);
      this.selectedProject = $event;
    }
  }

  addProject() {
    let selectableElement = new SelectableElement(this.addedProject, null, null);
    this.addModalProjectVisible = false;
    let project = new Project();
    project.name = this.addedProject;
    this.readProjectModel.push(project);
    this.middleCardComponentProject.addElement(selectableElement);
    this.middleCardComponentProject.postProcessAddProject(selectableElement);
    //this.middleCardComponentProject.modelCategoriesChanged()
  }

  closeProject() {
    this.addModalProjectVisible = false;
  }

  ngOnInit(): void {
    this.projectsService.getProjects().subscribe((projects) => {
      projects.forEach(project => {
        this.middleCardComponentProject.addElement(new SelectableElement(project.name, project.id, null));
      })
      this.readProjectModel = projects;
    })
  }

  saveSelectedProject() {
    if (this.selectedProject) {
      const project = new Project();
      project.id = this.selectedProject.id;
      project.name = this.selectedProject.description;
      project.labels = this.middleCardComponent.getElements().map(e => e.description);
      project.trainCases = this.middleCardComponentLearn.getElements().map(e => new TrainTestCase(e.label, e.description));
      project.testCases = this.middleCardComponentTest.getElements().map(e => new TrainTestCase(e.label, e.description));
      if (project.id) {
        this.projectsService.updateProject(project).subscribe((project) => {
          this.realoadProjects();
        })
      } else {
        this.projectsService.createProject(project).subscribe((project) => {
          this.realoadProjects();
        })
      }
    }
  }

  private realoadProjects() {
    this.selectedProject = null;
    this.middleCardComponentProject.middleTitleButtonComponent.selectedElement = null;
    this.middleCardComponentProject.setAllElements([]);
    this.middleCardComponent.setAllElements([]);
    this.middleCardComponentLearn.setAllElements([]);
    this.middleCardComponentTest.setAllElements([]);
    this.ngOnInit();

  }

  testEventPushed() {
    if (this.selectedProject) {
      const filteredModel = this.readProjectModel.filter(project => project.name === this.selectedProject.description);
      if (filteredModel.length == 1 && filteredModel[0].id) {
        this.projectsService.testProject(filteredModel[0]).subscribe((project) => {
          console.log('TESTED');
          this.realoadProjects();
        });
      } else {
        console.log('Condition not met');
      }
    }
  }

  trainEventPushed() {
    if (this.selectedProject) {
      const filteredModel = this.readProjectModel.filter(project => project.name === this.selectedProject.description);
      if (filteredModel.length == 1 && filteredModel[0].id) {
        this.projectsService.trainProject(filteredModel[0]).subscribe((project) => {
          console.log('TRAINED');
        });
      } else {
        console.log('Condition not met');
      }
    }
  }

  addedFileEventTestPushed($event: File) {
    console.log('test file upload ' + $event.name);
    const formData = new FormData();
    formData.append('file', $event);
    if ($event.name.endsWith('.pdf')) {

      this.projectsService.uploadFilePdf(formData).subscribe((resp) => {

      });

    } else {


      this.projectsService.uploadFile(formData).subscribe((resp) => {

      });
    }

  }

  addedFileEventUceniPushed($event: File) {
    console.log('train file upload ' + $event.name);
    const formData = new FormData();
    formData.append('file', $event);
    if ($event.name.endsWith('.pdf')) {

      this.projectsService.uploadFilePdf(formData).subscribe((resp) => {

      });

    } else {


      this.projectsService.uploadFile(formData).subscribe((resp) => {

      });
    }

  }
}
