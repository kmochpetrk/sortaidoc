import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MiddleTitleButtonComponent} from "../middle-title-button/middle-title-button.component";
import {SelectableElement} from "../model";

@Component({
  selector: 'app-middle-card',
  templateUrl: './middle-card.component.html',
  styleUrls: ['./middle-card.component.css']
})
export class MiddleCardComponent implements OnInit {

  @Input()
  public title;
  @Input()
  public buttonTitle;

  @ViewChild('mtbutton',{static: false})
  middleTitleButtonComponent: MiddleTitleButtonComponent;

  @Output()
  public buttonPushed = new EventEmitter();

  @Output()
  public buttonTimesPushed = new EventEmitter();

  @Output()
  public selectedElemChanged = new EventEmitter();

  @Output()
  public addedFileEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  pushButton() {
    this.buttonPushed.emit();
  }

  addElement(elem: SelectableElement) {
    this.middleTitleButtonComponent.elements.push(elem);
    //this.postProcessAddProject(elem);
  }

  public postProcessAddProject(elem: SelectableElement) {
    if (this.title === 'Projekt') {
      this.middleTitleButtonComponent.setSelectElement(elem);
    }
  }

  pushTimesButton(model: SelectableElement[]) {
    this.buttonTimesPushed.emit(model);
  }

  modelCategoriesChanged(modelNew: string[]) {
    this.middleTitleButtonComponent.categs = modelNew;
  }

  getElements(): SelectableElement[] {
    return this.middleTitleButtonComponent.elements;
  }

  selectElemChanged($event: SelectableElement) {
    this.selectedElemChanged.emit($event)
  }

  setAllElements(newModel: SelectableElement[]) {
    this.middleTitleButtonComponent.setElements(newModel);
  }

  fileAddedEventPushed($event: File) {
    this.addedFileEvent.emit($event);
  }
}
