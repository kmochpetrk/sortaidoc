import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddleTitleButtonComponent } from './middle-title-button.component';

describe('MiddleTitleButtonComponent', () => {
  let component: MiddleTitleButtonComponent;
  let fixture: ComponentFixture<MiddleTitleButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddleTitleButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddleTitleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
