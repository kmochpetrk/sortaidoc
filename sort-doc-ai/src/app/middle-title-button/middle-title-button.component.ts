import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectableElement} from "../model";

@Component({
  selector: 'app-middle-title-button',
  templateUrl: './middle-title-button.component.html',
  styleUrls: ['./middle-title-button.component.css']
})
export class MiddleTitleButtonComponent implements OnInit {

  @Input()
  public title;
  @Input()
  public buttonTitle;

  selectedElement: SelectableElement;

  public categs = [];

  public elements: SelectableElement[] = [];

  @Output()
  public buttonPushed = new EventEmitter();

  @Output()
  public buttonTimesPushed = new EventEmitter();

  @Output()
  public selectedElementChanged = new EventEmitter();

  @Output()
  public fileAddedEvent = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  pushedButton() {
    console.log('level 1 pushed');
    this.buttonPushed.emit();
  }

  pushTimes(elem: SelectableElement) {
    this.elements = this.elements.filter(filtered => filtered.description !== elem.description);
    if (this.selectedElement && (this.selectedElement.description === elem.description)) {
      this.selectedElement = null;
    }
    this.buttonTimesPushed.emit(this.elements);
  }

  evalVisibilityButton(): boolean {
    return (this.title !== 'Uceni') && (this.title !== 'Test');
  }

  addFiles =  (event: Event) => {
    console.log((event.target as HTMLInputElement).value);
    //   .forEach((file) => {
    //   this.elements.push(file.name);
    // })
    const fileSelected: File = (event.target as HTMLInputElement).files[0];
    this.elements.push(new SelectableElement((event.target as HTMLInputElement).files[0].name, null, null));
    this.fileAddedEvent.emit(fileSelected);
  };

  evalVisibilityTest(): boolean {
    return this.title === 'Test';
  }

  getFileId() {
    'fileId' + this.title;
  }

  isElementSelected(oneElement: SelectableElement) {
    if (!this.selectedElement) {
      return false;
    }
    return oneElement.description === this.selectedElement.description;
  }

  setSelectElement(oneElement: SelectableElement) {
    this.selectedElement = oneElement;
    this.selectedElementChanged.emit(oneElement);
  }

  setElements(newModel: SelectableElement[]) {
    this.elements = newModel;
  }
}
